package ru.kazakov.jse49.constant;

public class TerminalConstant {

    public static final String COM_EXIT = "exit";
    public static final String COM_VISA_USA = "visa-usa";
    public static final String COM_VISA_UK = "visa-uk";

}
