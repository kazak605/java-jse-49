package ru.kazakov.jse49.sender;

/**
 * Отправляет данные формы потребителю.
 */
public interface FormDataSender {
    /**
     * Отправляет данные формы.
     * @param data данные формы.
     */
    void send(String data);
}
