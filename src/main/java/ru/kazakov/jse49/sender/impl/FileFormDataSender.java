package ru.kazakov.jse49.sender.impl;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.kazakov.jse49.sender.FormDataSender;

@Log4j2
@Component
public class FileFormDataSender implements FormDataSender {

    @Override
    public void send(String data) {
        log.info(data);
    }

}
