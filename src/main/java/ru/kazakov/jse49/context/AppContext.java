package ru.kazakov.jse49.context;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "ru.kazakov.jse49")
public class AppContext {
}
