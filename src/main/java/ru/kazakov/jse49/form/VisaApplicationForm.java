package ru.kazakov.jse49.form;

/**
 * Форма данных заявки на визу.
 */
public interface VisaApplicationForm {
    /**
     * Запрашивает и заполняет данные формы.
     */
    void fillForm();

    /**
     * Возвращает данные формы.
     */
    String getFormData();
}
