package ru.kazakov.jse49.form.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.kazakov.jse49.command.CommandInterface;
import ru.kazakov.jse49.form.VisaApplicationForm;
import ru.kazakov.jse49.model.VisaForm;

@Component
@Scope("prototype")
public class VisaFormUSA implements VisaApplicationForm {

    private final CommandInterface commandInterface;

    private VisaForm visaForm;

    public VisaFormUSA(CommandInterface commandInterface) {
        this.commandInterface = commandInterface;
    }

    @Override
    public void fillForm() {
        visaForm = VisaForm.builder()
                .firstName(commandInterface.writeAndRead("Введите имя: "))
                .lastName(commandInterface.writeAndRead("Введите фамилию: "))
                .age(Integer.parseInt(commandInterface.writeAndRead("Введите возраст: ")))
                .visa("USA")
                .build();
    }

    @Override
    public String getFormData() {
        return visaForm.toString();
    }
}
