package ru.kazakov.jse49.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VisaForm {

    private String firstName;
    private String lastName;
    private int age;
    private String visa;

    @Override
    public String toString() {
        return "VisaForm{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", visa='" + visa + '\'' +
                '}';
    }
}
