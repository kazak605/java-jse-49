package ru.kazakov.jse49.command.impl;

import ru.kazakov.jse49.command.CommandInterface;

public class VoiceCommandInterfaceImpl implements CommandInterface {
    @Override
    public String writeAndRead(String text) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void write(String text) {
        throw new UnsupportedOperationException();
    }
}
