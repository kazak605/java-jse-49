package ru.kazakov.jse49.command.impl;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import ru.kazakov.jse49.command.CommandInterface;

import java.util.Scanner;

@Log4j2
@Component
@Primary
public class ConsoleCommandInterfaceImpl implements CommandInterface {

    Scanner scanner = new Scanner(System.in);

    @Override
    public String writeAndRead(String text) {
        log.info(text);
        return scanner.nextLine();
    }

    @Override
    public void write(String text) {
        log.info(text);
    }

}
