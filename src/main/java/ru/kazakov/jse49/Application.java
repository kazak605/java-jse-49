package ru.kazakov.jse49;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.kazakov.jse49.context.AppContext;
import ru.kazakov.jse49.process.ApplicationProcess;

@Log4j2
public class Application {

    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppContext.class);
        ApplicationProcess applicationProcess = (ApplicationProcess) ctx.getBean(ApplicationProcess.class);
        applicationProcess.process();
    }

}
