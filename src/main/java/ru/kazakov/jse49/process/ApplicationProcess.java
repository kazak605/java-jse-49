package ru.kazakov.jse49.process;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kazakov.jse49.command.CommandInterface;
import ru.kazakov.jse49.form.impl.VisaFormUSA;
import ru.kazakov.jse49.form.impl.VisaFromUK;
import ru.kazakov.jse49.sender.FormDataSender;
import ru.kazakov.jse49.constant.TerminalConstant;

@Log4j2
@Component
public class ApplicationProcess {

    public static final String EXIT = "exit";
    private String visaData = "";
    private CommandInterface commandInterface;
    private FormDataSender formDataSender;

    @Autowired
    public ApplicationProcess(CommandInterface commandInterface, FormDataSender formDataSender) {
        this.commandInterface = commandInterface;
        this.formDataSender = formDataSender;
    }

    public void process() {
        String command = "";
        while (!TerminalConstant.COM_EXIT.equals(command)) {
            command = commandInterface.writeAndRead("Укажите тип визы: USA или UK?");
            switch (command) {
                case TerminalConstant.COM_VISA_USA:
                    VisaFormUSA visaFormUSA = new VisaFormUSA(commandInterface);
                    visaFormUSA.fillForm();
                    visaData = visaFormUSA.getFormData();
                    break;
                case TerminalConstant.COM_VISA_UK:
                    VisaFromUK visaFromUK = new VisaFromUK(commandInterface);
                    visaFromUK.fillForm();
                    visaData = visaFromUK.getFormData();
                    break;
                case TerminalConstant.COM_EXIT:
                    return;
                default:
                    commandInterface.write("Комманда не найдена");
            }
            if (!visaData.isEmpty()) {
                formDataSender.send(visaData);
                visaData = "";
            }

        }
    }

}
